#!/usr/bin/env bash


########################## FOR MORE INFO
#### https://opentelemetry.io/docs/languages/python/automatic/
#### https://github.com/open-telemetry/opentelemetry-python-contrib/tree/main/opentelemetry-instrumentation
########## Exporters
export OTEL_EXPORTER_OTLP_TRACES_PROTOCOL=http/protobuf
export OTEL_TRACES_EXPORTER=${AUTO_OTEL_TRACES_EXPORTER-"console,otlp"}
export OTEL_METRICS_EXPORTER=${AUTO_OTEL_METRICS_EXPORTER-console}
export OTEL_LOGS_EXPORTER=${AUTO_OTEL_LOGS_EXPORTER-"console,otlp"}
########## Logging
export OTEL_PYTHON_LOG_CORRELATION=${AUTO_OTEL_LOG_CORRELATION-true}
export OTEL_PYTHON_LOG_LEVEL=${AUTO_OTEL_LOG_LEVEL-error}
export OTEL_PYTHON_LOG_FORMAT=${AUTO_OTEL_LOG_FMT-"%(asctime)s %(levelname)s [%(name)s] [%(filename)s:%(lineno)d] [trace_id=%(otelTraceID)s span_id=%(otelSpanID)s resource.service.name=%(otelServiceName)s trace_sampled=%(otelTraceSampled)s] - %(message)s"}
export OTEL_PYTHON_LOGGING_AUTO_INSTRUMENTATION_ENABLED=${AUTO_OTEL_AUTO_LOG_INSTRUMENTATION_ENABLED-true}
########## FASTAPI
export OTEL_PYTHON_FASTAPI_EXCLUDED_URLS=${AUTO_OTEL_EXCLUDED_URLS-health}

######

opentelemetry-instrument \
    --service_name ${API_TRACER_NAME-gateway-dflt} \
    --exporter_otlp_endpoint ${API_TRACE_BACKEND_URL-0.0.0.0:4318} \
    ${PYTHON_CMD-python3} ${APP_PATH-'/app/main.py'}


#### Optional flags instead of ENV Vars for setting exporters
    # --exporter_otlp_protocol http/protobuf \
    # --traces_exporter ${OTEL_TRACES_EXPORTER} \
    # --metrics_exporter ${OTEL_METRICS_EXPORTER} \
    # --logs_exporter ${OTEL_LOGS_EXPORTER} \