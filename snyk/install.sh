#!/usr/bin/env bash

function install_snyk_linux(){
    check_command_exists curl

    curl https://static.snyk.io/cli/latest/snyk-linux -o snyk
    chmod +x ./snyk
    mv ./snyk /usr/local/bin/ 
    rm -rf ./snyk

    check_command_exists snyk
}

function install_snyk_npm(){
    check_command_exists npm

    npm install -g snyk
    
    check_command_exists snyk
}