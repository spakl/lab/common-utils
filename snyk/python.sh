#!/usr/bin/env bash

function snyk_pip_scan(){
    check_command_exists snyk

    if [[ -z "${SNYK_ORG}" ]]; then
        log ERROR "FAIL: SNYK_ORG not set"
    fi

    if [[ -z "${SNYK_ENV}" ]]; then
        log ERROR "FAIL: SNYK_ENV not set"
    fi

    if [[ -z "${SNYK_REPORT_OUTPUT}" ]]; then
        log WARN "WARN: SNYK_REPORT_OUTPUT not set, defaulting to snyk.json"
    fi

    log INFO "Logging into snyk org: ${SNYK_ORG}"

    SNYK_CFG_ORG=${SNYK_ORG}
    snyk auth $SNYK_TOKEN
    snyk monitor \
        --command=python3 \
        --org=$SNYK_ORG \
        --project-lifecycle=${SNYK_ENV} \
        --project-environment=backend \
        --project-tags=${SNYK_TAGS-"team=proteus"}

    snyk test \
        --org=$SNYK_ORG \
        --command=python3 \
        --json-file-output=${SNYK_REPORT_OUTPUT-snyk.json} || echo "Forcing the scan to pass, the snyk-filter will fail if any findings high or above are found"

    check_command_exists snyk-to-html
    check_command_exists snyk-filter

    ## Generate html file from the json report
    snyk-to-html -i ${SNYK_REPORT_OUTPUT} -o snyk_report.html

    ## Running the  filter against the json file, this will fail the pipeline if there are any findings
    snyk-filter -i ${SNYK_REPORT_OUTPUT} -f ${SNYK_FILTER_FILEPATH-./config/snyk-filter.yml}

}