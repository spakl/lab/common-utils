#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source DIR/logger.sh
source DIR/utils.sh
source DIR/snyk/snyk.sh

