#!/usr/bin/env bash


log() {
  export TERM=xterm-256color

  if [[ -z "$LOG_LEVEL" ]]; then
      LOG_LEVEL="INFO"
  fi

    # Define colors
  GREEN='\033[0;32m'
  YELLOW='\033[1;33m'
  RED='\033[0;31m'
  NC='\033[0m' # No Color

    # Define log levels
  INFO="INFO"
  WARN="WARN"
  ERROR="ERROR"
  DEBUG="DEBUG"

  local log_level=$1
  local message=$2
  local timestamp="$(date '+%Y-%m-%d %H:%M:%S')"
  local filename="$(basename "$0")"

    # Check if log level is enabled
  if [[ "$log_level" == "$DEBUG" && "$LOG_LEVEL" != "$DEBUG" ]]; then
    return 0
  elif [[ "$log_level" == "$INFO" && "$LOG_LEVEL" != "$DEBUG" && "$LOG_LEVEL" != "$INFO" ]]; then
    return 0
  elif [[ "$log_level" == "$WARN" && "$LOG_LEVEL" == "$ERROR" ]]; then
    return 0
  fi

  # Define log file
  if [[ -z "${LOG_FILE}" ]]; then 
    log_file="$HOME/clogger.log"
  else 
    log_file=$LOG_FILE
  fi

  # Check if log file exists, if not create it and set permissions
  if [[ ! -e "$log_file" ]]; then
    touch "$log_file"
    chmod 644 "$log_file"
  fi

  # Check if the script can write to the log file
  if [[ ! -w "$log_file" && ! -O "$log_file" ]]; then 
    echo -e "${RED}Error: Cannot write to log file $log_file. Check permissions or run with elevated privileges.${NC}"
    exit 1
  fi

    # Print to console
  case $log_level in
    $INFO)
      echo -e "${GREEN}[$timestamp $filename][$log_level] $message${NC}"
      ;;
    $WARN)
      echo -e "${YELLOW}[$timestamp $filename][$log_level] $message${NC}"
      ;;
    $ERROR)
      echo -e "${RED}[$timestamp $filename][$log_level] $message${NC}"
      ;;
    $DEBUG)
      echo "[$timestamp $filename][$log_level] $message"
      ;;
    *)
      echo "[$timestamp $filename][UNKNOWN] $message"
      ;;
  esac
  # Write to log file
  echo "[$timestamp $filename][$log_level] $message" >> "$log_file"
}

function clog(){
  # Define log file
  if [[ -z "${LOG_FILE}" ]]; then
    log_file="$HOME/clogger.log"
  else 
    log_file=$LOG_FILE
  fi
  
  rm -f $log_file
  echo -e "${GREEN}[clog] logfile at $log_file successfully deleted${NC}"
}
