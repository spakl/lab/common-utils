#!/usr/bin/env bash

function check_command_exists(){
    log INFO "Testing to see if '$1' exists..."
    if command -v $1 >/dev/null 2>&1; then
        log INFO "$1 is installed"
    else
        log ERROR "$1 is not installed";
        exit 1;
    fi
}


